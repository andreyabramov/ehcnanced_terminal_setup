# README #


#Enchancing OS-X terminal

###If you're here that means you are only 10 minutes away from a new experience on your trusty Mac terminal should you follow this instructional. 
###Here you'll find tools that supercharge the default OS-X terminal with the Solarized theme, custom colors and extra functionality.

### ![picture](example_image.png) ###

####It is based on the follwing tools:

* [Solarized color shceme by Ethan Schoonover](http://ethanschoonover.com/solarized)  
* [Bash profile setup by Nathaniel Landau](https://natelandau.com/my-mac-osx-bash_profile/)
* [GNU coreutils](http://www.gnu.org/software/coreutils/manual/) - this is a dependency


### How do I get set up? ###
Note: The setup has been tested on a OS-X 10.11.05 El Capitan, but should work on 10.09+

1. Installing the Solarized terminal theme on OS-X:
	* Open a terminal and select `Import` under the `Shell` menu or press `command+O` .
	* Select one of the two themes supplied in the `xterm-256color` dir. Note: this setup is optimized for dark, but light works too.
	* Open terminal preferences or press `command+`, .
	* Select `Profiles` then select the newly installed theme from the list and click the `default` button on the lower left corner. <br>

2. Installing enchanced terminal functionality profile script
	* Download the supplied utilities.bash file in a location of your choosing (eg `~/scripts/utilities.bash`)
	* Source the script in you `.profile` or `.bashrc` (e.g. add `source /path/to/script` to your `.profile`) <br>

3. Enabling custom colors
	* You need to install 'coreutils' for the custom colors to work.
	* This can be obtained through MacPorts using "sudo port isntall coreutils" or with a package manager of your choice.
	* Copy over the dir\_colors file supplied to `~/.dir-colors` <br>
	
4. Enjoy the magic!
	
Each one of those 3 steps can be completed separately, e.g have custom colours without the Solarized theme and enanced terminal functionality.

Note: If you only want the custom colours you need to add the following to your .profile in addition to step 3 above:
```
eval 'gdircolors ~/.dir_colors'
alias ls="gls --color=auto"                #Use GNU ls and add custom colors
alias ll="gls --color=auto -lh"
alias la="gls --color=auto -a"
```
These lines come supplied with utilities.bash so you don't need to add them if you've completed step 2.

You change the color scheme for file attributes and file extensions by editing the dir\_colors file. 
Limited instructions are available inside the file.

Andrey Abramov